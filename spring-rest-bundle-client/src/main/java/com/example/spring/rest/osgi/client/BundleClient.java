package com.example.spring.rest.osgi.client;

import java.io.IOException;

import com.example.spring.rest.osgi.dto.BundleDTO;

public interface BundleClient {

	public BundleDTO getBundle(long bundleId) throws IOException;

    public String whoAreYou();

}
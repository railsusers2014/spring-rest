package com.example.spring.rest.osgi.client.impl;

import java.io.IOException;
import java.net.URL;

import org.codehaus.jackson.map.ObjectMapper;

import com.example.spring.rest.osgi.client.BundleClient;
import com.example.spring.rest.osgi.dto.BundleDTO;


public final class BundleClientImpl implements BundleClient {
	private static final String BASE_URL = "http://localhost:8181";
	private static final String CONTEXT = "/spring-rest";

	public BundleDTO getBundle(long bundleId) throws IOException {
        URL url = new URL(BASE_URL + CONTEXT + "/bundle/" + bundleId);
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(url.openStream(), BundleDTO.class);
	}

    @Override
    public String whoAreYou() {
        return "I'm Bundle Client Impl";
    }
}
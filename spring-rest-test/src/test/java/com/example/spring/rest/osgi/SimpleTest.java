package com.example.spring.rest.osgi;

import com.example.spring.rest.osgi.client.BundleClient;
import com.example.spring.rest.osgi.util.OsgiUtil;
import org.eclipse.osgi.internal.serviceregistry.ServiceReferenceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.ops4j.pax.exam.Configuration;
import org.ops4j.pax.exam.Option;
import org.ops4j.pax.exam.junit.PaxExam;
import org.ops4j.pax.exam.spi.reactors.ExamReactorStrategy;
import org.ops4j.pax.exam.spi.reactors.PerClass;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

import javax.inject.Inject;
import java.io.IOException;

import static org.junit.Assert.assertTrue;

@RunWith(PaxExam.class)
@ExamReactorStrategy(PerClass.class)
public class SimpleTest {

    @Inject
    private BundleContext bundleContext;

    @Configuration
    public Option[] config() {
        return OsgiUtil.confg();
    }

    @Test
    public void testIt() throws IOException {

        System.out.println("Hello world");

        assertTrue(true);
    }
}

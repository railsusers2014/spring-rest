package com.example.spring.rest.osgi;

import com.example.spring.rest.osgi.client.BundleClient;
import org.apache.karaf.features.FeaturesService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.ops4j.pax.exam.Configuration;
import org.ops4j.pax.exam.Option;
import org.ops4j.pax.exam.junit.PaxExam;
import org.ops4j.pax.exam.spi.reactors.ExamReactorStrategy;
import org.ops4j.pax.exam.spi.reactors.PerClass;

import javax.inject.Inject;

import static org.junit.Assert.assertEquals;
import static org.ops4j.pax.exam.CoreOptions.maven;
import static org.ops4j.pax.exam.karaf.options.KarafDistributionOption.features;
import static org.ops4j.pax.exam.karaf.options.KarafDistributionOption.karafDistributionConfiguration;

@RunWith(PaxExam.class)
@ExamReactorStrategy(PerClass.class)
public class FullFeaturesTest {

    @Inject
    private BundleClient bundleClient;

    @Inject
    private FeaturesService featureService;

    @Configuration
    public Option[] config() {
        return new Option[]{
                karafDistributionConfiguration()
                        .frameworkUrl(maven()
                                .groupId("org.apache.karaf")
                                .artifactId("apache-karaf").type("zip")
                                .versionAsInProject())
                        .karafVersion("2.2.4")
                        .name("Apache Karaf"),
                features(maven()
                        .groupId("com.example").artifactId("spring-rest-features")
                        .version("0.0.1-SNAPSHOT").type("xml")
                        .classifier("features")
                        .versionAsInProject(),
                        "spring-rest-client")
        };
    }

    @Test
    public void testIt() {
        System.out.println(bundleClient.whoAreYou());
        assertEquals("I'm Bundle Client Impl", bundleClient.whoAreYou());
    }
}

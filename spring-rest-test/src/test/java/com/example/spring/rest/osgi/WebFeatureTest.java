package com.example.spring.rest.osgi;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.ops4j.pax.exam.Configuration;
import org.ops4j.pax.exam.Option;
import org.ops4j.pax.exam.junit.PaxExam;
import org.ops4j.pax.exam.spi.reactors.ExamReactorStrategy;
import org.ops4j.pax.exam.spi.reactors.PerClass;
import org.ops4j.pax.exam.util.Filter;
import org.springframework.web.context.WebApplicationContext;

import javax.inject.Inject;

import static org.junit.Assert.assertNotNull;
import static org.ops4j.pax.exam.CoreOptions.maven;
import static org.ops4j.pax.exam.karaf.options.KarafDistributionOption.features;
import static org.ops4j.pax.exam.karaf.options.KarafDistributionOption.karafDistributionConfiguration;

@RunWith(PaxExam.class)
@ExamReactorStrategy(PerClass.class)
public class WebFeatureTest {

    @Configuration
    public Option[] config() {
        return new Option[]{
                karafDistributionConfiguration()
                        .frameworkUrl(maven()
                                .groupId("org.apache.karaf")
                                .artifactId("apache-karaf").type("zip")
                                .versionAsInProject())
                        .karafVersion("3.0.0")
                        .name("Apache Karaf"),
                features(maven()
                        .groupId("com.example").artifactId("spring-rest-features")
                        .version("0.0.1-SNAPSHOT").type("xml")
                        .classifier("features")
                        .versionAsInProject(),
                        "spring-rest")
        };
    }

    @Test
    public void testIt() throws InterruptedException {
        System.out.println("Waiting...");
        Thread.sleep(200000);
    }

}
